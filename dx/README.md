# Developer Experience

From the [RFC](/dx/rfc):

> Developer Experience (DX) is a set of practices covered around cloud native environments that give organizations and teams unified, cross-functional representation, shared accountability from development, operations, the business and everybody in between.
> 
> An operating model for DX provides a set of best practices that unify deployment, management and monitoring for containerized clusters and applications. A path towards a DX for managing applications; where end-to-end CICD pipelines and Git workflows are applied to both operations, and development.

In summary, DX is how we call the newly proposed best practices for developing and deploying software at Datopian. In Google Cloud, we have a whole new project called "datopian-dx," with a new Kubernetes cluster that we hope to replace the current one.

## DX

### Deploy a New Application

See [Deploy &raquo;](./deploy).

### Run A Data Migration Between Environments

See [Data Migration &raquo;](./data-migration).

## Cluster

### How Do It Create The Cluster

It's under [How to Use the Developer Experience (DX) Setup](/dx/cluster/#create-the-cluster).

### Install Argo CD in the Cluster

It's under [How to Use the Developer Experience (DX) Setup](/dx/cluster/#install-argo-cd-in-the-cluster).
